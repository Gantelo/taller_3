#!/usr/bin/env python3

# -*- coding: utf-8 -*-

"""

Created on Sat Sep 29 15:00:16 2018



@author: giuliano

"""

#Importo los módulos necesarios y defino mi archivo de entrada, de salida, y mi ventana:
import datetime
import sys 
entrada = (open(sys.argv[1]))
ventana = (int(sys.argv[3]))

#La siguiente función agarra mi archivo de salida, lo procesa sacándole el caracter escondido \n, y pone todo en filas uniendo con ','. Además, convierte los valores registrados por sensores en float
def mat(ent):
    matriz = []
    for lista in ent:
        fila = lista.rstrip('\n').split(',')
        #rstrip('\n') va a sacarle el '\n'del final del gran elemento
        #split(',') va a separar los elementos por las divisiones por comas
        matriz.append(fila)
#Lo siguiente va a hacer que me transforme los elementos de la matriz de str a float, salteandose a los timestamp y a los valores NA
    listaflotada=[]
    for sublista in matriz:
        sublistaflotada=[]
        for i in sublista:
            if i==sublista[0] or i== 'NA':
                sublistaflotada.append(i)
            else:
                sublistaflotada.append(float(i))
        listaflotada.append(sublistaflotada)
    return listaflotada

#Asigno una variable al resultado de esta función para facilitar el anidado más adelante
matriz= mat (entrada)
#print (matriz)

#La siguiente función me va a calcular los promedios tal y como pide el ejercicio.
def promedios (matriz, ventana):
    #Defino los bordes (top, bottom) de mi ventana el indice j de mis columnas de la matriz y la lista donde van a ir a parar los resultados (resu)
    bottom=0
    top= ventana
    j=1 #indice de columnas
    #Largo de las columnas:
    n_col = len(matriz[0])-1 
    resu=[]
    
    #Restrinjo el borde superior de la ventana al numero de filas de mi matriz
    while top<=len (matriz):
        promedios=[]
        while j <= n_col:
            datos=[]
            i=bottom
            #pido que i (indice de filas de la matriz) se mueva solo dentro de la ventana, tome los datos de cada columna, y los guarde en una lista intermediaria
            while i< top: #es menor y no menor igual porque python cuenta desde 0
                datos.append(matriz[i][j])
                i+=1
            suma=0
           
            #Pido que, si no hay ningún NA, se sumen los datos tomados
            if noFaltanDatos(datos):
                for i in datos:
                    suma= suma + i
                promedio=suma/len(datos)
                promedio= '{:.2f}'.format(promedio)
                promedios.append(promedio)
            else:
                promedios.append('NA')
            #Una vez que se tomaron todos los datos de este set, pido que se mueva a la siguiente columna
            j+=1
        #Cuando se tomaron todos los datos a esta altura de las columnas, vuelve a empezar moviendo la ventana una unidad, y que me guarde los datos del set anterior en una lista dentro de resu
        resu.append(promedios)
        j=1 #vuelvo al primer sensor
        top += 1 #corro la ventana 1 lugar
        bottom += 1    
        
    return resu     
#print (promedios(matriz,sys.argv[3]))  #Ejemplo para ver como funciona

def noFaltanDatos(a): #funcion que evalúa recursivamente si falta algun dato en la ventana tomada para cada sensor
    if len(a)>0:
        if a[0]!= 'NA':
            return noFaltanDatos(a[1:])
        else: return False
    if len(a)==0:
        return True
        
# La función Cronos lo que hace es volver sobre el archivo de entrada y tomar los timestamp (que hasta ahora se fueron salteando), y guardarlos en una lista time
#Luego, dependiendo de la ventana tomada, va a agarrar a ciertos tiempos dentro de time y le hace la diferencia. Va guardando todo en otra lista.
def cronos(entrada, ventana):
    time=[]
    n=0
    while n < len (entrada):
        tiempo= datetime.datetime.strptime(entrada[n][0], '%Y-%m-%dT%H:%M:%S')
        time.append(tiempo)
        n+=1
        
    bottom=0 

    #como python cuenta desde 0, le resto 1 a "ventana", que es el tamaño que quiero que tenga mi ventana (es el "n" del ejemplo del taller)
    ventana=ventana-1
    top=ventana
    intervalos=[]
    while top<(len(time)):
        t1=time[bottom]
        t2=time[top]
        rangot=(t2-t1).total_seconds()
        rangot= str (rangot)
        intervalos.append(rangot)
        bottom+=1
        top+=1
    return intervalos

#print (cronos (matriz, 2))

#print (promedios (matriz, 2))

#La funcion merge va a agarrar los intervalos sacados en cronos y meterlos en el return de promedios en el orden adecuado
#Soy conciente que esto podria haberse metido en cronos pero la fui dividiendo para entender bien lo que está pasando
    
def merge(promedios, cronos):
    for lista in promedios:
        n=0
        while len(cronos)!=0:
            promedios[n].insert(0,cronos.pop(0))
            n+=1
    return promedios

#Anido todas las funciones anteriores y le asigno una variable:
    
salidita=(merge(promedios(matriz,ventana),cronos(matriz,ventana)))

#Finalmente, matsaliente me configura mi matriz de salida según el formato pedido
def matsaliente(salida):
    final =[]
    for lista in salida:
        c=','.join(lista)
        final.append(c)
    finalenfila= '\n'.join(final)
    return finalenfila

#print (matsaliente(salida))

#Configuro el archivo de salida...
salida= open(sys.argv[2],'w')
print ((matsaliente(salidita)), file=salida)
salida.close()

#Fin!